package com.example.myapplication;

public class NewsModel {

     private  String title;
     private  String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public String getExperect() {
        return description.substring(0,60);
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
