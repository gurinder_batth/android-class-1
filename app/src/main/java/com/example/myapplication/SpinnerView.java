package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SpinnerView extends AppCompatActivity {

    Spinner spinner;
    String default_value = "12th Class";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_view);
        spinner = (Spinner) findViewById(R.id.spinner);

        List<String> list =  new ArrayList<String>();
        list.add("Select Drop Down");
        list.add("8th Class");
        list.add("10th Class");
        list.add("12th Class");
        list.add("Grudation");
        list.add("Master");
        list.add("Phd");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,list);
        spinner.setAdapter(adapter);
        spinner.setSelection(3);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String val  = parent.getSelectedItem().toString();
                if( default_value.equals(val) == false ){
                    Toast.makeText(SpinnerView.this, val, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}