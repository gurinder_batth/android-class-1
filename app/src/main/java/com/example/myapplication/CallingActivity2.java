package com.example.myapplication;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import java.net.URI;

public class CallingActivity2 extends AppCompatActivity {


    String premssion[] = {Manifest.permission.CALL_PHONE };
    int request_code = 200;
    Intent intent;
    Boolean clicked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailer);
        requestPermissions( premssion , request_code );
    }


    public void call(View view){
        clicked = true;
        intent = new Intent();
        intent.setAction(Intent.ACTION_CALL);
        intent.setData( Uri.parse("tel:+919646848434") );
        if( checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED ){
            startActivity(intent);
        }
        else if(  shouldShowRequestPermissionRationale(premssion[0]) == false ){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Oops! Something went wrong");
            builder.setMessage("We Need phone call permission to use this app");
            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent setting = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,Uri.parse("package:"+BuildConfig.APPLICATION_ID));
                    startActivity(setting);
                }
            });
            builder.show();
        }
        else{
            requestPermissions( premssion , request_code );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED  && clicked == true){
                    startActivity(intent);
                }
                break;
        }
    }
}