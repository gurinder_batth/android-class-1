package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText username,pwd,pwd_con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = (EditText) findViewById(R.id.username);
        pwd = (EditText) findViewById(R.id.password);
        pwd_con = (EditText) findViewById(R.id.password_confirmation);
    }

    public void loginUser(View view){

                 String  _username = username.getText().toString();
                 String  _pwd = pwd.getText().toString();
                 String  _pwd_con = pwd_con.getText().toString();

                 if( ( _username.length() >= 5 && _username.length() <= 15 ) == false ){
                     Toast.makeText(this, "Username length should be between 5 to 15 chars", Toast.LENGTH_SHORT).show();
                     return ;
                 }


                if( ( _pwd.length() >= 5 && _pwd.length() <= 10 ) == false ){
                    Toast.makeText(this, "Password length should be between 5 to 15 chars", Toast.LENGTH_SHORT).show();
                    return ;
                }


                if( ( _pwd_con.length() >= 5 && _pwd_con.length() <= 10 ) == false ){
                    Toast.makeText(this, "Password Confirmation length should be between 5 to 15 chars", Toast.LENGTH_SHORT).show();
                    return ;
                }



                if(   _pwd.equals(_pwd_con)   == false ){
                       Toast.makeText(this, "Password don't match", Toast.LENGTH_SHORT).show();
                       return ;
                }



                 Intent i = new Intent( this , HelloWorld.class );
                 i.putExtra("username", _username );
                 startActivity( i );
                 finish();


    }

}