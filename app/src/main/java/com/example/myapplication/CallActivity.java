package com.example.myapplication;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Toast;

import java.net.URI;

public class CallActivity extends AppCompatActivity {


    String premssion[] = {Manifest.permission.CALL_PHONE };
    int request_code = 200;
    Intent intent;
    Boolean clicked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        requestPermissions( premssion , request_code );
    }


    public void call(View view){
        clicked = true;
        intent = new Intent();
        intent.setAction(Intent.ACTION_CALL);
        intent.setData( Uri.parse("tel:+919646848434") );
        if( checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED ){
            startActivity(intent);
        }else{
            requestPermissions( premssion , request_code );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                  if( shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE) == false &&  clicked == true){
                      AlertDialog.Builder alert = new AlertDialog.Builder(this);
                      alert.setTitle("Oops!");
                      alert.setMessage("Please give premission");
                      alert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {

                          }
                      });
                      alert.show();
                  }
                  if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED  && clicked == true){
                      startActivity(intent);
                  }
                 break;
        }
    }
}