package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class NewSpinner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_spinner);
        Spinner spinner  = (Spinner) findViewById(R.id.select_type);
        spinner.setSelected(false);
        List<String> l = new ArrayList<String>();
        l.add("Select Dropdown");
        l.add("+2");
        l.add("Graduation");
        l.add("Master");
        l.add("Phd");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item,l);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(NewSpinner.this, position + " " , Toast.LENGTH_SHORT).show();
//                    adapter.remove(spinner.getSelectedItem().toString());
//                    adapter.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(NewSpinner.this, "Not Selected" , Toast.LENGTH_SHORT).show();
            }
        });

    }
}