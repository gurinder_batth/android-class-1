package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;

public class Dailer extends AppCompatActivity {

    Intent intent;

    String per[] = { Manifest.permission.CALL_PHONE , Manifest.permission.VIBRATE };
    int request_code = 200;
    boolean dailer = false;
    TextView screen_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailer);
        requestPermissions( per , request_code );
        screen_number = (TextView) findViewById(R.id.screen_number);
    }


    public void phone(View view){
         Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
         vibe.vibrate(100);
         switch (view.getId()){
             case R.id.v0:
                 screen_number.setText(  screen_number.getText().toString() + "0");
                 break;
             case R.id.v1:
                 screen_number.setText(  screen_number.getText().toString() + "1" );
                 break;
             case R.id.v2:
                 screen_number.setText(  screen_number.getText().toString() + "2" );
                 break;
             case R.id.v3:
                 screen_number.setText( screen_number.getText().toString() + "3" );
                 break;
             case R.id.v4:
                 screen_number.setText(  screen_number.getText().toString() + "4" );
                 break;
             case R.id.v5:
                 screen_number.setText(  screen_number.getText().toString() + "5" );
                 break;
             case R.id.v6:
                 screen_number.setText(  screen_number.getText().toString() + "6");
                 break;
             case R.id.v7:
                 screen_number.setText(  screen_number.getText().toString() + "7");
                 break;
             case R.id.v8:
                 screen_number.setText( screen_number.getText().toString() + "8");
                 break;
             case R.id.v9:
                 screen_number.setText( screen_number.getText().toString() + "9" );
                 break;
             case R.id.clr:
                 String old = screen_number.getText().toString();
                 if( old.length() == 0 ){
                     return;
                 }
                 screen_number.setText( old.substring( 0 , old.length() -1 ) );
                 break;
         }


    }


    public void dail(View view){
//           intent = new Intent();
//           intent.setAction( Intent.ACTION_DIAL );
//           intent.setData( Uri.parse("tel:+919646848434") );
//           startActivity(intent);

           dailer = true;
           intent = new Intent();
           intent.setAction( Intent.ACTION_CALL );
           intent.setData( Uri.parse("tel:" + screen_number.getText().toString() ) );


           if(checkCallingOrSelfPermission(per[0]) == PackageManager.PERMISSION_GRANTED ){
                startActivity(intent);
           }else if( shouldShowRequestPermissionRationale(per[0]) == false){

               AlertDialog.Builder builder = new AlertDialog.Builder(this);
               builder.setTitle("Oops! Something went Wrong");
               builder.setMessage("Oops! App Required Phone Permission");
               builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                        Intent setting = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,Uri.parse("package:"+ BuildConfig.APPLICATION_ID ));
                        startActivity(setting);
                   }
               });
               builder.show();
           }

           else{
                   requestPermissions( per , request_code );
           }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

         switch (requestCode){
                 case 200:
                     if(  grantResults.length > 0 ){
                         if( grantResults[0] ==  PackageManager.PERMISSION_GRANTED && dailer == true ){
                               startActivity(intent);
                         }
                     }
                     break;
         }

    }

}