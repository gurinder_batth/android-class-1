package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationHolder;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.custom.CustomErrorReset;
import com.basgeekball.awesomevalidation.utility.custom.CustomValidation;
import com.basgeekball.awesomevalidation.utility.custom.CustomValidationCallback;
import com.basgeekball.awesomevalidation.utility.custom.SimpleCustomValidation;

public class Class4Validation extends AppCompatActivity {


    RadioGroup gender;
    RadioButton gender_selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class4_validation);
        gender = (RadioGroup) findViewById(R.id.gender);
    }

    public void submit(View View){

//        \w{3,10}
//        \w+@\w+\.\w{2,5}
        gender_selected = (RadioButton) findViewById( gender.getCheckedRadioButtonId() );
        if(gender_selected != null){
//            Toast.makeText(this, gender_selected.getText().toString(), Toast.LENGTH_SHORT).show();
        }
        AwesomeValidation awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this,R.id.username,"\\w{3,10}",R.string.error_username);
        awesomeValidation.addValidation(this,R.id.username,"\\w{3,10}",R.string.error_username);

        awesomeValidation.addValidation(this, R.id.gender, new CustomValidation() {
                 @Override
                  public boolean compare(ValidationHolder validationHolder) {
                        RadioGroup r = (RadioGroup) validationHolder.getView();
//                        Toast.makeText(Class4Validation.this, r.getCheckedRadioButtonId() + "" , Toast.LENGTH_SHORT).show();
                         if(r.getCheckedRadioButtonId() == -1){
                             return false;
                         }
                         return  true;
                    }
                },
                new CustomValidationCallback() {
                    @Override
                    public void execute(ValidationHolder validationHolder) {
                        Toast.makeText(Class4Validation.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                },
                new CustomErrorReset() {
                    @Override
                    public void reset(ValidationHolder validationHolder) {
                        Toast.makeText(Class4Validation.this, "Reset", Toast.LENGTH_SHORT).show();
                    }
                } ,
                R.string.gender);
        awesomeValidation.addValidation(this,R.id.email,"\\w+@\\w+\\.\\w{3,5}",R.string.error_email);
        awesomeValidation.addValidation(this, R.id.city, new SimpleCustomValidation() {
            @Override
            public boolean compare(String s) {
                if(s.equalsIgnoreCase("Ludhiana") || s.equals("Delhi")){
                    return true;
                }
                return false;
            }
        },R.string.city_error);
        if( awesomeValidation.validate() ){
            Toast.makeText(this, "Awesome", Toast.LENGTH_SHORT).show();
        }


    }


}