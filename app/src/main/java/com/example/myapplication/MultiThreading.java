package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MultiThreading extends AppCompatActivity {

    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_threading);
        textView2 = (TextView) findViewById(R.id.textView2);
    }

    public void pressButton(View view){

//        MyThreading myThreading = new MyThreading();
//        myThreading.start();

          CustomThread customThread = new CustomThread();
          Thread t1 = new Thread(customThread);
          t1.start();

    }


    class CustomThread implements  Runnable {

        @Override
        public void run() {
            for(int i = 0 ; i <10 ; i++) {
                Log.v("printing_", "<<" + i + ">>");
                if(i == 5){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView2.setText("50%");
                        }
                    });
                }
            }
        }
    }

    class MyThreading extends  Thread{
        @Override
        public void run() {
            for(int i = 0 ; i <10 ; i++){
                Log.v("printing_","<<"+i+">>");
                if(i == 5){
//                    Handler handler = new Handler(Looper.getMainLooper());
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            textView2.setText("50%");
//                        }
//                    });

//                    textView2.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            textView2.setText("50%");
//                        }
//                    });
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            textView2.setText("50%");
//                        }
//                    });

                }
                try{
                    Thread.sleep(1000);
                }catch(Exception e){

                }
            }
        }



    }
}