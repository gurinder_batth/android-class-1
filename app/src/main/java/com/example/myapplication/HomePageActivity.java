package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class HomePageActivity extends AppCompatActivity {

    RecyclerView recyclerView;
//    LinearLayout mainlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
//        mainlayout = (LinearLayout) findViewById(R.id.mainlayout);
        List<String> list = new ArrayList<String>();
        list.add("Apple");
        list.add("Batman");
        list.add("Catwoman");
        list.add("Dog");
        list.add("Elephant");
//        mainlayout.addView(view);
//        for( int i = 0 ; i < list .size() ; i++ ){
//            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.card_item,mainlayout,false);
//            TextView t =  view.findViewById(R.id.item_text);
//            t.setText(list.get(i).toString());
//            mainlayout.addView(view);
//        }

        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        recyclerView.setAdapter( new  CustomRecycleView(list) );
    }


     class CustomRecycleView extends  RecyclerView.Adapter<CustomViewHolder> {

         public List list;
         CustomRecycleView(List list){
              this.list = list;
         }

         @Override
         public int getItemCount() {
             return this.list.size();
         }

         @NonNull
         @Override
         public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
             View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,parent,false);
             CustomViewHolder customViewHolder = new CustomViewHolder(view);
             return customViewHolder;
         }

         @Override
         public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
             holder.textView.setText( list.get( position ).toString() );
         }
     }

      public class CustomViewHolder extends RecyclerView.ViewHolder{

            public TextView textView;
            public CustomViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.item_text);
            }
      }
}