package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);

        NewsModel n1 = new NewsModel();
        n1.setTitle("India won’t be silenced: Rahul Gandhi reacts to Disha Ravi’s arrest in toolkit case");
        n1.setDescription("Like Rahul Gandhi, many others have expressed concern after the arrest of the climate activist, who was sent to 5-day police custody on Sunday. It has triggered widespread outrage in the country, with academics, activists and opposition party politicians calling for her immediate release. ");

        NewsModel n2 = new NewsModel();
        n2.setTitle("March to Nabanna: Injured Left leader dies, sparks political slugfest in Bengal");
        n2.setDescription("A Left youth leader who was injured in a clash with the police during the Left parties' march to West Bengal secretariat Nabanna on February 11 succumbed to his injuries on Monday morning, igniting a political row in the state.");


        List<NewsModel> list = new ArrayList<NewsModel>();
        list.add(n1);
        list.add(n2);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        recyclerView.setAdapter(new HomeActivity.MyRecylerView(list));
    }


      public class  MyRecylerView extends RecyclerView.Adapter<MyHolder> {


       private List<NewsModel> list;

       public MyRecylerView(List<NewsModel> list) {
           this.list = list;
       }

        @NonNull
        @Override
        public HomeActivity.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,parent,false);
            return new MyHolder(view);
        }

        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
              holder.textView.setText( list.get(position).getTitle() );
              holder.description.setText( list.get(position).getExperect() );
        }

        @Override
        public int getItemCount() {
            return this.list.size();
        }
    }

     public class  MyHolder  extends  RecyclerView.ViewHolder{

        public TextView textView, description;

        MyHolder(View view){
            super(view);
            this.textView = (TextView) view.findViewById(R.id.item_text);
            this.description = (TextView) view.findViewById(R.id.item_description);
        }

    }


}