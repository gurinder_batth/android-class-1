package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.custom.SimpleCustomValidation;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;
import static com.basgeekball.awesomevalidation.ValidationStyle.COLORATION;

public class ValidationLib extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation_lib);
    }

    public void submit(View view){
        AwesomeValidation mAwesomeValidation = new AwesomeValidation(COLORATION);
        mAwesomeValidation.addValidation(this, R.id.username, "[\\w]{3,}", R.string.error_username);
        mAwesomeValidation.addValidation(this, R.id.email, "\\w+@\\w+\\.\\w+", R.string.error_email);

        mAwesomeValidation.addValidation(this, R.id.birthday, new SimpleCustomValidation() {
            @Override
            public boolean compare(String s) {
                if(s.equals("Test")){
                    return true;
                }
                return false;
            }
        },R.string.error_email);


        if (mAwesomeValidation.validate()) {
            Toast.makeText(this, "Validation Successfull", Toast.LENGTH_LONG).show();
        }
    }
}