package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

public class TestActivitySession extends AppCompatActivity {

    String skey = "mykey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_test_session);
        SharedPreferences sharedPreferences = getSharedPreferences(skey, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String username = sharedPreferences.getString("username","test");
        if( username.equals("admin@admin.com") ){

            Toast.makeText(this, "Yupyyyyyyyyyyyyyyyyyy! Youre Logined", Toast.LENGTH_SHORT).show();
            editor.remove("username");
           // editor.clear();
            editor.commit();

        }else{
            editor.putString("username","admin@admin.com");
            editor.commit();
            Toast.makeText(this, "You are not logined", Toast.LENGTH_SHORT).show();

        }


    }
}