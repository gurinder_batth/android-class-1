package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

public class SessionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        SharedPreferences sharedPreferences = getSharedPreferences("my_shared", Context.MODE_PRIVATE);
        String user = sharedPreferences.getString("_user","Gurinder");


        if(user.equals("admin@admin.com")){
            Toast.makeText(this, "User Logined", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("_user");
            editor.apply();
        }else{
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("_user","admin@admin.com");
            editor.apply();
            Toast.makeText(this, "User not Login", Toast.LENGTH_SHORT).show();
        }
    }
}