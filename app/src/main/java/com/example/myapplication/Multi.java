package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Multi extends AppCompatActivity {
    TextView f;
    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi);
        f = findViewById(R.id.text);
    }

    public void start(View view){
        MyThread2 myThread = new MyThread2();
        Thread t = new Thread(myThread);
        t.start();
    }

    class MyThread extends  Thread {
        @Override
        public void run() {
            for(int i = 0; i < 10 ; i++){
                Log.v("printing_",i+"");
                try{
                    Thread.sleep(500);
                }catch(Exception e){

                }
            }
        }
    }


    class MyThread2 implements  Runnable {
        public void run() {
            for(int i = 0; i < 10 ; i++){
                Log.v("printing_",i+"");
                try{
                    if(i == 5){
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                f.setText("50%");
//                            }
//                        });

                        f.post(new Runnable() {
                            @Override
                            public void run() {
                                f.setText("50%");
                            }
                        });
                    }
                    Thread.sleep(500);

                }catch(Exception e){

                }
            }
        }
    }
}