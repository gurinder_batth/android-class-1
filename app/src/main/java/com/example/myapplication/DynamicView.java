package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DynamicView extends AppCompatActivity {

    LinearLayout mainlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_view);
        mainlayout = (LinearLayout) findViewById(R.id.mainlayout);
        for(int i = 0; i < 5 ; i++){
            final int inci = i;
            View v1 = LayoutInflater.from(this).inflate(R.layout.sayhi,mainlayout,false);
            TextView t1 = (TextView) v1.findViewById(R.id.hi);
            t1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(DynamicView.this, inci + "" , Toast.LENGTH_SHORT).show();
                }
            });
            t1.setText("Happy New Year 2021");
            mainlayout.addView(t1);
        }
    }
}